# WicketHea - a programming challenge

## Execution
This application is build using maven and packaged as war file, thus it may be deployed into an tomcat application server. Because it is based on spring boot, it contains an embedded tomcat and it can be run as stand alone application as well.

### Requirements
- Java 7
- Maven 3.5
- MySql database
- Network access (for downloading maven dependencies)

### Build
- Open a console window for this folder
- Run `mvn install`

### Configure
The database connection is configured by the file `/wickethea/src/main/resources/application.properties` using these properties with these default values:
`spring.datasource.url=jdbc:mysql://localhost:3306/wickethea?useSSL=false`
`spring.datasource.username=mysql`
`spring.datasource.password=password`

If you want to run the prebuild war, you have to edit the file within the archive. It is located in the containing folder 'WEB-INF\classes\'.

### Run
1. Run using maven
	Run `mvn spring-boot:run` 
	Open browser with `http://localhost:8080`
2. Run war from console using embedded tomcat
	Run `java -jar .\target\wickethea.war`
	Open browser with `http://localhost:8080`
3. Deploy war to tomcat server
	Move `webmon.war` into tomcat webapps folder
	Open browser (default: `http://localhost:8080/wickethea`)
	
## Concept
### Frameworks
This application is build primarily by using these frameworks: 
- Spring Boot 
- Hibernate
- Wicket 7.9.0 (The earliest stable release supported with spring boot integration)
- Jersey 