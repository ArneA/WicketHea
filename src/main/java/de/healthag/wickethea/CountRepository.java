package de.healthag.wickethea;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

public interface CountRepository extends CrudRepository<Count, Date> {

}
