package de.healthag.wickethea.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;

import de.healthag.wickethea.CountService;

/**
 * The wicket page.
 *
 * @author Arne
 * @since 13.02.2018
 *
 */
@WicketHomePage
public class HomePage extends WebPage {

	private static final long serialVersionUID = -8898204223243743199L;

	@SpringBean
	private CountService countService;

	public HomePage() {
		add(new Label("date", countService.getCurrentDate()));

		Form<Object> form = new Form<>("form");

		form.add(new Label("count", new AbstractReadOnlyModel<Integer>() {
			private static final long serialVersionUID = -6579347742017168727L;

			@Override
			public Integer getObject() {
				return countService.getCurrentCount();
			}
		}));

		form.add(new Button("button") {
			private static final long serialVersionUID = -962914779845449363L;

			public void onSubmit() {
				countService.getAndIncrementCurrentCount();
			};
		});

		add(form);
	}
}
