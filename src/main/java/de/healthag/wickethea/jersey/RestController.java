package de.healthag.wickethea.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.healthag.wickethea.CountService;

/**
 * The rest controller for accessing the count service.
 *
 * @author Arne
 * @since 13.02.2018
 *
 */
@Component
@Path("/count")
public class RestController {

	@Autowired
	private CountService countService;

	@GET
	@Produces("application/json")
	public String count() {
		Integer count = countService.getAndIncrementCurrentCount();
		return count.toString();
	}
}
