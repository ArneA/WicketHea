package de.healthag.wickethea;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementing the count functionality.
 *
 * @author Arne
 * @since 13.02.2018
 *
 */
@Service
public class CountService {

	@Autowired
	private CountRepository repository;

	public Date getCurrentDate() {
		return Date.valueOf(LocalDate.now());
	}

	public Integer getCurrentCount() {
		Count count = repository.findOne(getCurrentDate());
		if (count == null) {
			return 0;
		}
		return count.getCount();
	}

	public Integer getAndIncrementCurrentCount() {
		Count count = repository.findOne(getCurrentDate());
		if (count == null) {
			count = new Count(getCurrentDate(), 0);
		}
		count.setCount(count.getCount() + 1);
		count = repository.save(count);
		return count.getCount();
	}
}
