package de.healthag.wickethea;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoIntegrationTest {
	// TODO: Verify need of transactions

	public static final Count COUNT1 = new Count(Date.valueOf("2018-02-12"), 4);

	@Autowired
	private CountRepository repository;

	@Test
	public void testRepoExists() {
		assertNotNull(repository);
	}

	@Test
	public void testRoundtrip() {
		Count saved = repository.save(COUNT1);
		assertEquals(COUNT1, saved);

		Count findOne = repository.findOne(COUNT1.getDate());
		assertEquals(COUNT1, findOne);
	}
}
