package de.healthag.wickethea;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountServiceIntegrationTest {

	@Autowired
	private CountService countService;

	@Test
	public void testServiceExists() {
		assertNotNull(countService);
	}

	@Test
	public void testGetCurrentDate() {
		Date dateFromService = countService.getCurrentDate();
		Date currentDate = Date.valueOf(LocalDate.now());
		assertEquals(currentDate, dateFromService);
	}

	@Test
	public void testGetCurrentCount() {
		Integer count = countService.getCurrentCount();
		assertEquals(0, count.intValue());
	}

	@Test
	public void testGetAndIncrementCurrentCount() {
		Integer count;
		count = countService.getAndIncrementCurrentCount();
		assertEquals(1, count.intValue());
		count = countService.getAndIncrementCurrentCount();
		assertEquals(2, count.intValue());
	}
}
